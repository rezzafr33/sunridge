<?php

// Make sure fusion-core-sunridge is before after fusion-core
function filter_active_plugins( $active_plugins ) 
{
	if(in_array('fusion-core-sunridge/fusion-core-sunridge.php', $active_plugins)) {
		$sunridge = array('fusion-core-sunridge/fusion-core-sunridge.php', 'fusion-core/fusion-core.php');
		$active_plugins = array_diff($active_plugins, $sunridge);
		array_unshift($active_plugins, $sunridge);
	}
    return $active_plugins;
};
        
add_filter( 'active_plugins', 'filter_active_plugins', 10, 1 );