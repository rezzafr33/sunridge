<?php
class FusionSC_ListGrid {

	public static $parent_args;
	public static $child_args;

	/**
	 * Initiate the shortcode
	 */
	public function __construct() {

		add_filter( 'fusion_attr_listgrid-shortcode', array( $this, 'attr' ) );
		add_filter( 'fusion_attr_listgrid-shortcode-icon', array( $this, 'icon_attr' ) );
		add_filter( 'fusion_attr_listgrid-shortcode-title', array( $this, 'title_attr' ) );

		add_shortcode( 'listgrid', array( $this, 'render_parent' ) );
		add_shortcode( 'listgrid-single', array( $this, 'render_child' ) );
	}

	/**
	 * Render the parent shortcode
	 * @param  array $args	 Shortcode paramters
	 * @param  string $content Content between shortcode
	 * @return string		  HTML output
	 */
	function render_parent( $args, $content = '') {

		$defaults = FusionCore_Plugin::set_shortcode_defaults(
			array(
				'class' 			=> '',
				'id' 				=> '',
			), $args
		);

		extract( $defaults );

		self::$parent_args = $defaults;
		
		$html = sprintf( '<div %s>', FusionCore_Plugin::attributes( 'listgrid-shortcode' ) );

		$html .= sprintf('<ul>%s</ul>', do_shortcode( $content ) );

		$html .= '</div>';

		return $html;

	}

	function attr() {

		$attr = array();

		// FIXXXME had clearfix class; group mixin working?
		$attr['class'] = 'listgrid';

		if( self::$parent_args['class'] ) {
			$attr['class'] .= ' ' . self::$parent_args['class'];
		}

		if( self::$parent_args['id'] ) {
			$attr['id'] = self::$parent_args['id'];
		}

		return $attr;

	}

	/**
	 * Render the child shortcode
	 * @param  array $args	 Shortcode paramters
	 * @param  string $content Content between shortcode
	 * @return string		  HTML output
	 */
	function render_child( $args, $content = '') {

		$defaults = FusionCore_Plugin::set_shortcode_defaults(
			array(
				'icon'        => '',
				'title'       => 'List Title',
				'content'     => '',
				'url'         => '#'
			), $args
		);

		extract( $defaults );

		self::$child_args = $defaults;
		
		$html = '<li>'.
					sprintf('<a href="%s">', $url) .
						(('' !== $icon) ? sprintf('<span><i %s></i></span>', FusionCore_Plugin::attributes('listgrid-shortcode-icon')) : '') .
						sprintf('<h4 %s>%s</h4>', FusionCore_Plugin::attributes('listgrid-shortcode-title'), $title) .
					'</a>' .
					(('' !== $content) ? sprintf('<p>%s</p>', $content) : '')  .
				'</li>';

		return $html;

	}

	function icon_attr() {
		$attr = array();
		$attr['class'] = sprintf('%s medium listgrid-icon-color', self::$child_args['icon']);

		return $attr;
	}

	function title_attr() {

		$attr = array();

		$attr['class'] = 'listgrid-title';

		if( '' == self::$child_args['icon'] ) {
			$attr['class'] .= ' no-icon';
		}

		return $attr;

	}

}

new FusionSC_ListGrid();