<?php
class FusionSC_Carousel {

	private $carousel_counter = 1;

	public static $parent_args;
	public static $child_args;

	/**
	 * Initiate the shortcode
	 */
	public function __construct() {

		add_filter( 'fusion_attr_carousel-shortcode', array( $this, 'attr' ) );
		add_filter( 'fusion_attr_carousel-shortcode-slide', array( $this, 'slide_attr' ) );
		add_filter( 'fusion_attr_carousel-shortcode-icon', array( $this, 'icon_attr' ) );

		add_action( 'wp_enqueue_scripts', array($this, 'carousel_scripts'));

		add_shortcode( 'carousel', array( $this, 'render_parent' ) );
		add_shortcode( 'carousel-single', array( $this, 'render_child' ) );
		add_action( 'init', array($this, 'full_image_as_default_filter') );
	}

	/**
	 * Render the parent shortcode
	 * @param  array $args	 Shortcode paramters
	 * @param  string $content Content between shortcode
	 * @return string		  HTML output
	 */
	function render_parent( $args, $content = '') {

		$defaults = FusionCore_Plugin::set_shortcode_defaults(
			array(
				'class' 			=> '',
				'id' 				=> '',
			), $args
		);

		extract( $defaults );

		self::$parent_args = $defaults;
		
		$html = sprintf( '<div %s>', FusionCore_Plugin::attributes( 'carousel-shortcode' ) );

		$html .= sprintf('<div class="mask"><div class="slideset">%s</div></div>', do_shortcode( $content ) );

		if($this->carousel_counter > 1) {
			$html .= '<div class="col-sm-12 aligncenter">' .
						'<a class="btn-prev" href="#"><span class="fa fa-chevron-left"></span></a>' .
						'<a class="btn-next" href="#"> <span class="fa fa-chevron-right"></span></a>' .
						'<ul class="pagination">';
						
			for ($i=1; $i < $this->carousel_counter; $i++) {
					$html .= "<li><a href='#'>${i}</a></li>";
			}

			$html .=    '</ul>' .
					 '</div>';
		}

		$html .= '</div>';

		return $html;

	}

	function attr() {

		$attr = array();

		// FIXXXME had clearfix class; group mixin working?
		$attr['class'] = 'carousel';

		if( self::$parent_args['class'] ) {
			$attr['class'] .= ' ' . self::$parent_args['class'];
		}

		if( self::$parent_args['id'] ) {
			$attr['id'] = self::$parent_args['id'];
		}

		return $attr;

	}

	/**
	 * Render the child shortcode
	 * @param  array $args	 Shortcode paramters
	 * @param  string $content Content between shortcode
	 * @return string		  HTML output
	 */
	function render_child( $args, $content = '') {

		$defaults = FusionCore_Plugin::set_shortcode_defaults(
			array(
				'icon'        => '',
				'title'       => 'Carousel Title',
				'content'     => 'Carousel Content',
				'url'         => '#',
				'button_text' => '',
			), $args
		);

		extract( $defaults );

		self::$child_args = $defaults;

		$button = ($button_text == '') ? '' : "<a href='${url}' class='carousel-button'>${button_text}</a>";

		$html =	sprintf('<div %s>', FusionCore_Plugin::attributes('carousel-shortcode-slide')) . '<article class="aligncenter">';

		if('' != $icon) 
			$html .= sprintf('<i %s></i>', FusionCore_Plugin::attributes('carousel-shortcode-icon'));

		$html .= 	 sprintf('<h2><a href="%s" class="carousel-title">%s</a></h2>', $url, $title) .
					 sprintf('<p>%s</p>', $content) .
					 sprintf('%s', $button) .
				'</article></div>';

		$this->carousel_counter++;

		wp_enqueue_script('fusion_carousel_shortcode');

		return $html;

	}

	//data-src="http://sunridge.dev/app/uploads/2015/05/img-1-large.jpg" data-width="900" data-height="444"
	function slide_attr() {
		$attr = array();
		$attr['class'] = 'slide item-' . $this->carousel_counter;

		return $attr;
	}

	function icon_attr() {
		$attr = array();
		$attr['class'] = sprintf('%s larger carousel-icon-color', self::$child_args['icon']);

		return $attr;
	}

	function carousel_scripts() {
		wp_register_script('fusion_carousel_shortcode', plugin_dir_url(dirname(__FILE__)) . 'assets/js/carousel.js', array('jquery'), true );
	}

	function full_image_as_default() {
		return 'full';
	}

	function full_image_as_default_filter() {
		add_filter( 'pre_option_image_default_size', array($this, 'full_image_as_default') );
	}
}

new FusionSC_Carousel();