<?php
class FusionSC_Testimonials_two {

	private $testimonials_carousel_counter = 0;
	private $testimonials_counter = 1;

	public static $parent_args;
	public static $child_args;

	/**
	 * Initiate the shortcode
	 */
	public function __construct() {

		add_filter( 'fusion_attr_testimonials2-shortcode', array( $this, 'attr' ) );
		add_filter( 'fusion_attr_testimonials2-shortcode-testimonial', array( $this, 'attr_testimonial' ) );

		add_action( 'wp_enqueue_scripts', array($this, 'testimonials_scripts'));

		add_shortcode( 'testimonials2', array( $this, 'render_parent' ) );
		add_shortcode( 'testimonial2', array( $this, 'render_child' ) );
	}

	/**
	 * Render the parent shortcode
	 * @param  array $args	 Shortcode paramters
	 * @param  string $content Content between shortcode
	 * @return string		  HTML output
	 */
	function render_parent( $args, $content = '') {
		global $smof_data;

		$defaults = FusionCore_Plugin::set_shortcode_defaults(
			array(
				'class' 			=> '',
				'id' 				=> '',
				'title'				=> '',
				'textcolor' 		=> strtolower( $smof_data['testimonial_text_color'] ),
			), $args
		);

		extract( $defaults );

		self::$parent_args = $defaults;

		$html =	sprintf("<div %s>", FusionCore_Plugin::attributes( 'testimonials2-shortcode'));

		$html .= (('' !== $title) ? sprintf('<h1 %s>%s</h1>', FusionCore_Plugin::attributes( 'testimonials2-shortcode-testimonial'), $title) : '' );

		$html .= sprintf("<div class='mask'><div class='slideset'>%s</div></div>", do_shortcode($content));

		if($this->testimonials_carousel_counter > 1) {
			$html .= '<ul class="pagination">';
			for ($i=1; $i <= $this->testimonials_carousel_counter; $i++) {
				$html .= "<li><a href='#'>${i}</a></li>";
			}
			$html .= '</ul>';
		}

		$html .=	"</div>";

		$this->testimonials_counter++;

		wp_enqueue_script('fusion_testimonials_shortcode');

		return $html;

	}

	function attr() {

		$attr = array();

		$attr['class'] = sprintf( 'testimonials testimonials-%s', $this->testimonials_counter );

		if (self::$parent_args['textcolor']) {
			$attr['style'] = sprintf('color:%s;', self::$parent_args['textcolor']);
		}

		if( self::$parent_args['class'] ) {
			$attr['class'] .= ' ' . self::$parent_args['class'];
		}

		if( self::$parent_args['id'] ) {
			$attr['id'] = self::$parent_args['id'];
		}

		return $attr;

	}

	/**
	 * Render the child shortcode
	 * @param  array $args	 Shortcode paramters
	 * @param  string $content Content between shortcode
	 * @return string		  HTML output
	 */
	function render_child( $args, $content = '') {

		$defaults = FusionCore_Plugin::set_shortcode_defaults(
			array(
				'link'					=> '',
				'name'	 				=> '',
				'target'				=> '_self',
			), $args
		);

		extract( $defaults );

		self::$child_args = $defaults;	

		$html =	'<div class="slide">' .
					sprintf('<blockquote %s>', FusionCore_Plugin::attributes( 'testimonials2-shortcode-testimonial')) .
						sprintf('<q %s>%s</q>', FusionCore_Plugin::attributes( 'testimonials2-shortcode-testimonial'), $content) .
						(('' !== $link) ? sprintf('<a href="%s">', $link) : '') .
						sprintf('<cite %s>%s</cite>', FusionCore_Plugin::attributes( 'testimonials2-shortcode-testimonial'), $name) .
						(('' !== $link) ? '</a>' : '') .
					'</blockquote>' .
				'</div>';

		$this->testimonials_carousel_counter++;

		return $html;

	}

	function attr_testimonial() {
		if (self::$parent_args['textcolor']) {
			$attr['style'] = sprintf('color:%s;', self::$parent_args['textcolor']);
		}
		return $attr;
	}

	function testimonials_scripts() {
		wp_register_script('fusion_testimonials_shortcode', plugin_dir_url(dirname(__FILE__)) . 'assets/js/testimonials.js', array('jquery'), true );
	}

}

new FusionSC_Testimonials_two();