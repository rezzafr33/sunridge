<?php
class FusionSC_Slideshow {

	private $slider_counter = 0;

	public static $parent_args;
	public static $child_args;

	/**
	 * Initiate the shortcode
	 */
	public function __construct() {

		add_filter( 'fusion_attr_slideshow-shortcode', array( $this, 'attr' ) );
		add_filter( 'fusion_attr_slideshow-shortcode-img', array( $this, 'img_attr' ) );
		add_filter( 'fusion_attr_slideshow-shortcode-img-ie', array( $this, 'img_attr_ie' ) );
		add_filter( 'fusion_attr_slideshow-shortcode-img-noscript', array( $this, 'img_attr_noscript' ) );
		add_filter( 'fusion_attr_slideshow-shortcode-button-url-data', array( $this, 'button_attr' ) );

		add_action( 'wp_enqueue_scripts', array($this, 'slideshow_scripts'));

		add_shortcode( 'slideshow', array( $this, 'render_parent' ) );
		add_shortcode( 'slideshow-single', array( $this, 'render_child' ) );
		add_action( 'init', array($this, 'full_image_as_default_filter') );

	}

	/**
	 * Render the parent shortcode
	 * @param  array $args	 Shortcode paramters
	 * @param  string $content Content between shortcode
	 * @return string		  HTML output
	 */
	function render_parent( $args, $content = '') {

		$defaults = FusionCore_Plugin::set_shortcode_defaults(
			array(
				'class' 			=> '',
				'id' 				=> '',
			), $args
		);

		extract( $defaults );

		self::$parent_args = $defaults;
		
		$html = sprintf( '<div %s>', FusionCore_Plugin::attributes( 'slideshow-shortcode' ) );

		$html .= sprintf('<div class="slideset">%s</div>', do_shortcode( $content ) );

		if($this->slider_counter > 1) {
			$html .= '<ul class="pagination-f">';
			for ($i=1; $i <= $this->slider_counter; $i++) {
				$html .= "<li><a href='#'>${i}</a></li>";
			}
			$html .= '</ul>';
		}

		$html .= '</div>';

		return $html;

	}

	function attr() {

		$attr = array();

		// FIXXXME had clearfix class; group mixin working?
		$attr['class'] = 'slideshow';

		if( self::$parent_args['class'] ) {
			$attr['class'] .= ' ' . self::$parent_args['class'];
		}

		if( self::$parent_args['id'] ) {
			$attr['id'] = self::$parent_args['id'];
		}

		return $attr;

	}

	/**
	 * Render the child shortcode
	 * @param  array $args	 Shortcode paramters
	 * @param  string $content Content between shortcode
	 * @return string		  HTML output
	 */
	function render_child( $args, $content = '') {

		$defaults = FusionCore_Plugin::set_shortcode_defaults(
			array(
				'title'       => 'Slide Title',
				'subtitle'    => 'Slide Subtitle',
				'content'     => 'Slide Content',
				'image'       => '',
				'button_text' => 'Learn More',
				'button_url'  => '#'
			), $args
		);

		extract( $defaults );

		if('' == $image) {
			return '';
		}

		self::$child_args = $defaults;
		
		$html = '<div class="slide">';

		$html .=	'<div class="img-block">' .
						'<span data-picture data-alt="image description">' .
							sprintf('<span %s></span>', FusionCore_Plugin::attributes('slideshow-shortcode-img')) .
							'<!--[if (lt IE 9) & (!IEMobile)]>' .
								sprintf('<span %s></span>', FusionCore_Plugin::attributes('slideshow-shortcode-img-ie')) .
							'<![endif]-->' .
							'<!-- Fallback content for non-JS browsers. Same img src as the initial, unqualified source element. -->' .
							sprintf('<noscript><img %s></noscript>', FusionCore_Plugin::attributes('slideshow-shortcode-img-noscript')) .
						'</span>' .
					'</div>';

		$html .=	'<div class="holder-c">' .
						'<article class="text">' .
							sprintf('<h1 class="color-blue">%s</h1>', $title) .
							sprintf('<strong class="subtitle">%s</strong>', $subtitle) .
							sprintf('<p>%s</p>', $content) .
							sprintf('<a %s>%s</a>', FusionCore_Plugin::attributes('slideshow-shortcode-button-url-data'), $button_text) .
						'</article>' .
					'</div>';

		$html .= '</div>';

		$this->slider_counter++;

		wp_enqueue_script('fusion_slideshow_shortcode');

		return $html;

	}

	//data-src="http://sunridge.dev/app/uploads/2015/05/img-1-large.jpg" data-width="900" data-height="444"
	function img_attr() {
		$attr = array();
		$attr['data-src'] = self::$child_args['image'];
		$attr['data-width'] = "900";
		$attr['data-height'] = "444";

		return $attr;
	}

	// data-src="images/img-1-large.jpg"
	function img_attr_ie() {
		$attr = array();
		$attr['data-src'] = self::$child_args['image'];

		return $attr;
	}

	// src="http://sunridge.dev/app/uploads/2015/05/img-1-large.jpg" data-width="900" data-height="444" alt="image description"
	function img_attr_noscript() {
		$attr = array();
		$attr['src'] = self::$child_args['image'];
		$attr['data-width'] = "900";
		$attr['data-height'] = "444";
		$attr['alt'] = self::$child_args['title'];

		return $attr;
	}

	function button_attr(){
		$attr = array();
		$attr['href'] = self::$child_args['button_url'];
		$attr['class'] = 'btn-more bg-primary';

		return $attr;
	}

	function slideshow_scripts() {
		wp_register_script('fusion_slideshow_shortcode', plugin_dir_url(dirname(__FILE__)) . 'assets/js/slideshow.js', array('jquery'), true );
	}

	function full_image_as_default() {
		return 'full';
	}

	function full_image_as_default_filter() {
		add_filter( 'pre_option_image_default_size', array($this, 'full_image_as_default') );
	}

}

new FusionSC_Slideshow();