<?php

require_once( FUSION_TINYMCE_DIR . '/config.php');


function sunridge_fusion_icons() {
	$pattern = '/\.(fusion-icon-(?:\w+(?:-)?)+):before\s+{\s*content:\s*"(.+)";\s+}/';
	$fusionicons_path = plugin_dir_url(dirname(__FILE__)) . 'assets/css/fusion-icon.css';
	
	@$subject = file_get_contents( $fusionicons_path );

	preg_match_all($pattern, $subject, $matches, PREG_SET_ORDER);

	$fusionicons = array();

	foreach($matches as $match){
		$fusionicons[$match[1]] = $match[1];
	}

	return $fusionicons;
}

$fusionicons = sunridge_fusion_icons();

$fusion_shortcodes['slideshow'] = array(
	'params' => array(
		'class' => array(
			'std' => '',
			'type' => 'text',
			'label' => __( 'CSS Class', 'fusion-core' ),
			'desc' => __( 'Add a class to the wrapping HTML element.', 'fusion-core' )
		),
		'id' => array(
			'std' => '',
			'type' => 'text',
			'label' => __( 'CSS ID', 'fusion-core' ),
			'desc' => __( 'Add an ID to the wrapping HTML element.', 'fusion-core' )
		)
	),
	'shortcode' => '[slideshow class="{{class}}" id="{{id}}"]{{child_shortcode}}[/slideshow]', // as there is no wrapper shortcode
	'popup_title' => __( 'Slider Shortcode', 'fusion-core' ),
	'no_preview' => true,

	// child shortcode is clonable & sortable
	'child_shortcode' => array(
		'params' => array(
			'title' => array(
				'std' => 'Slide Title',
				'type' => 'text',
				'label' => __( 'Slide Title', 'fusion-core' ),
				'desc' => __('Add the url of where more button will link to.', 'fusion-core')
			),
			'subtitle' => array(
				'std' => 'Slide Subtitle',
				'type' => 'text',
				'label' => __( 'Slide Subtitle', 'fusion-core' ),
				'desc' => __('Add the url of where more button will link to.', 'fusion-core')
			),
			'content' => array(
				'std' => __('Slide Content', 'fusion-core'),
				'type' => 'textarea',
				'label' => __( 'Slide Content', 'fusion-core' ),
				'desc' => __( 'Slide content', 'fusion-core' )
			),
			'image' => array(
				'std' => '',
				'type' => 'uploader',
				'label' => __( 'Slide Image', 'fusion-core' ),
				'desc' => __('Upload an image to display in the slide', 'fusion-core')
			),
			'button_text' => array(
				'std' => 'Learn More',
				'type' => 'text',
				'label' => __( 'More Button Text', 'fusion-core' ),
				'desc' => __('Text on button.', 'fusion-core')
			),
			'button_url' => array(
				'std' => '#',
				'type' => 'text',
				'label' => __( 'More Button Link', 'fusion-core' ),
				'desc' => __('Add the url of where more button will link to.', 'fusion-core')
			),
		),
		'shortcode' => '[slideshow-single title="{{title}}" subtitle="{{subtitle}}" content="{{content}}" image="{{image}}" button_text="{{button_text}}" button_url="{{button_url}}"]',
		'clone_button' => __( 'Add New Slide', 'fusion-core')
	)
);

$fusion_shortcodes['carousel'] = array(
	'params' => array(
		'class' => array(
			'std' => '',
			'type' => 'text',
			'label' => __( 'CSS Class', 'fusion-core' ),
			'desc' => __( 'Add a class to the wrapping HTML element.', 'fusion-core' )
		),
		'id' => array(
			'std' => '',
			'type' => 'text',
			'label' => __( 'CSS ID', 'fusion-core' ),
			'desc' => __( 'Add an ID to the wrapping HTML element.', 'fusion-core' )
		)
	),
	'shortcode' => '[carousel class="{{class}}" id="{{id}}"]{{child_shortcode}}[/carousel]', // as there is no wrapper shortcode
	'popup_title' => __( 'Slider Shortcode', 'fusion-core' ),
	'no_preview' => true,

	// child shortcode is clonable & sortable
	'child_shortcode' => array(
		'params' => array(
			'icon' => array(
				'type' => 'iconpicker',
				'label' => __( 'Select Icon class', 'fusion-core' ),
				'desc' => __( 'Custom setting only. Click an icon to select, click again to deselect', 'fusion-core' ),
				'options' => $fusionicons
			),
			'title' => array(
				'std' => 'Carousel Title',
				'type' => 'text',
				'label' => __( 'Carousel Title', 'fusion-core' ),
				'desc' => __('Add the url of where more button will link to.', 'fusion-core')
			),
			'content' => array(
				'std' => __('Carousel Content', 'fusion-core'),
				'type' => 'textarea',
				'label' => __( 'Carousel Content', 'fusion-core' ),
				'desc' => __( 'Carousel content', 'fusion-core' )
			),
			'url' => array(
				'std' => '#',
				'type' => 'text',
				'label' => __( 'Get More Info', 'fusion-core' ),
				'desc' => __('Add the url of where more button will link to.', 'fusion-core')
			),
			'button_text' => array(
				'std' => 'Learn More',
				'type' => 'text',
				'label' => __( 'More Button Text', 'fusion-core' ),
				'desc' => __('Text on button.', 'fusion-core')
			),
		),
		'shortcode' => '[carousel-single icon="{{icon}}" title="{{title}}" content="{{content}}" url="{{url}}" button_text="{{button_text}}"]',
		'clone_button' => __( 'Add New Carousel', 'fusion-core')
	)
);

$fusion_shortcodes['listgrid'] = array(
	'params' => array(
		'class' => array(
			'std' => '',
			'type' => 'text',
			'label' => __( 'CSS Class', 'fusion-core' ),
			'desc' => __( 'Add a class to the wrapping HTML element.', 'fusion-core' )
		),
		'id' => array(
			'std' => '',
			'type' => 'text',
			'label' => __( 'CSS ID', 'fusion-core' ),
			'desc' => __( 'Add an ID to the wrapping HTML element.', 'fusion-core' )
		)
	),
	'shortcode' => '[listgrid class="{{class}}" id="{{id}}"]{{child_shortcode}}[/listgrid]', // as there is no wrapper shortcode
	'popup_title' => __( 'Slider Shortcode', 'fusion-core' ),
	'no_preview' => true,

	// child shortcode is clonable & sortable
	'child_shortcode' => array(
		'params' => array(
			'icon' => array(
				'type' => 'iconpicker',
				'label' => __( 'Select Icon class', 'fusion-core' ),
				'desc' => __( 'Custom setting only. Click an icon to select, click again to deselect', 'fusion-core' ),
				'options' => $fusionicons
			),
			'title' => array(
				'std' => 'List Title',
				'type' => 'text',
				'label' => __( 'Carousel Title', 'fusion-core' ),
				'desc' => __('Add the url of where more button will link to.', 'fusion-core')
			),
			'content' => array(
				'std' => __('', 'fusion-core'),
				'type' => 'textarea',
				'label' => __( 'Carousel Content', 'fusion-core' ),
				'desc' => __( 'Carousel content', 'fusion-core' )
			),
			'url' => array(
				'std' => '#',
				'type' => 'text',
				'label' => __( 'Url', 'fusion-core' ),
				'desc' => __('Add the url of where list.', 'fusion-core')
			)
		),
		'shortcode' => '[listgrid-single icon="{{icon}}" title="{{title}}" content="{{content}}" url="{{url}}"]',
		'clone_button' => __( 'Add New List', 'fusion-core')
	)
);

$fusion_shortcodes['testimonials_2'] = array(
	'no_preview' => true,
	'params' => array(
		'title' => array(
			'std' => '',
			'type' => 'text',
			'label' => __( 'Title', 'fusion-core' ),
			'desc' => __( 'Insert testimonials title.', 'fusion-core' ),
		),
		'textcolor' => array(
			'type' => 'colorpicker',
			'std' => '',
			'label' => __( 'Text Color', 'fusion-core' ),
			'desc' => __( 'Controls the text color. Leave blank for theme option selection.', 'fusion-core' ),
		),
		'class' => array(
			'std' => '',
			'type' => 'text',
			'label' => __( 'CSS Class', 'fusion-core' ),
			'desc' => __( 'Add a class to the wrapping HTML element.', 'fusion-core' )
		),
		'id' => array(
			'std' => '',
			'type' => 'text',
			'label' => __( 'CSS ID', 'fusion-core' ),
			'desc' => __( 'Add an ID to the wrapping HTML element.', 'fusion-core' )
		),		
	),
	'shortcode' => '[testimonials2 title="{{title}}" textcolor="{{textcolor}}" class="{{class}}" id="{{id}}"]{{child_shortcode}}[/testimonials2]',
	'popup_title' => __( 'Insert Testimonials Shortcode', 'fusion-core' ),

	'child_shortcode' => array(
		'params' => array(
			'name' => array(
				'std' => '',
				'type' => 'text',
				'label' => __( 'Name', 'fusion-core' ),
				'desc' => __( 'Insert the name of the person.', 'fusion-core' ),
			),
			'link' => array(
				'std' => '',
				'type' => 'text',
				'label' => __( 'Link', 'fusion-core' ),
				'desc' => __( 'Add the url the company name will link to.', 'fusion-core' ),
			),
			'target' => array(
				'type' => 'select',
				'label' => __( 'Target', 'fusion-core' ),
				'desc' => __( '_self = open in same window <br />_blank = open in new window.', 'fusion-core' ),
				'options' => array(
					'_self' => '_self',
					'_blank' => '_blank'
				)
			),
			'content' => array(
				'std' => '',
				'type' => 'textarea',
				'label' => __( 'Testimonial Content', 'fusion-core' ),
				'desc' => __( 'Add the testimonial content', 'fusion-core' ),
			)
		),
		'shortcode' => '[testimonial2 name="{{name}}" link="{{link}}" target="{{target}}"]{{content}}[/testimonial2]',
		'clone_button' => __( 'Add Testimonial', 'fusion-core' )
	)
);