(function(document, window, $) {

	$('.slideshow').each(function(index, el) {
		$(this).fadeGallery({
			slides: 'div.slide',
			btnPrev: 'a.btn-prev',
			btnNext: 'a.btn-next',
			pagerLinks: '.pagination-f li',
			event: 'click',
			useSwipe: true,
			autoRotation: true,
			autoHeight: true,
			switchTime: 3000,
			animSpeed: 500
		});
	});

})(document, window, jQuery);