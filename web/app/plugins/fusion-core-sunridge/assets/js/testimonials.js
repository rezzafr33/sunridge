(function(document, window, $) {

	$('.testimonials').each(function(index, el){
		$(this).scrollAbsoluteGallery({
			mask: 'div.mask',
			slider: 'div.slideset',
			slides: 'div.slide',
			btnPrev: 'a.btn-prev',
			btnNext: 'a.btn-next',
			pagerLinks: '.pagination li',
			stretchSlideToMask: true,
			maskAutoSize: true,
			autoRotation: true,
			switchTime: 3000,
			animSpeed: 500
		});

	});

})(document, window, jQuery);