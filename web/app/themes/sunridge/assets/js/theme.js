(function(document, window, $) {
	$('.fancybox').fancybox({
		helpers: {

		    // Enable the media helper to better handle video.
		    media: true,

		    // Put comments within the white border.
		    title: {
		        type: 'inside'
		    }
		},

		// Do not use this because it tries to fit title text as well.
		fitToView: false,

		// Prevent the introduction of black bars when resized for mobile.
		aspectRatio: true,

		// Restrict content to the display dimensions.
		maxWidth: "100%",
		maxHeight: "100%",

		// Change the title keyword to 'caption' to avoid title text in tooltips.
		beforeLoad: function() {
		    this.title = $(this.element).attr('caption');
		},

		// Override the default iframe dimensions with manually set dimensions.
		afterLoad: function() {
		    this.width = $(this.element).data("width");
		    this.height = $(this.element).data("height");
		}


	});


})(document, window, jQuery);