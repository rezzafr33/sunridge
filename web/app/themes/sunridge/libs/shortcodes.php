<?php

function sunridge_mobile_logo() {
	ob_start();
	?>
	<div class="fusion-mobile-logo">
		<a href="<?php echo home_url(); ?>">
			<?php
			$logo_url = fusion_get_theme_option( 'mobile_logo' );
			
			if ( $logo_url ) {
				$logo_size = fusion_get_attachment_data_by_url( $logo_url, 'logo' );

				if ( ! $logo_size ) {
					$logo_size = getimagesize( $logo_url );

					if ( $logo_size ) {
						$logo_size['width'] = $logo_size[0];
						$logo_size['height'] = $logo_size[1];
					} else {
						$logo_size['width'] = '';
						$logo_size['height'] = '';
					}
				}
			} else {
				$logo_size['width'] = '';
				$logo_size['height'] = '';				
			}
			?>			
			<img src="<?php echo fusion_get_theme_option( 'mobile_logo' ); ?>" width="<?php echo $logo_size['width']; ?>" height="<?php echo $logo_size['height']; ?>" alt="<?php bloginfo( 'name' ); ?>" class="fusion-logo-1x fusion-standard-logo" />
			<?php
			$retina_logo = fusion_get_theme_option( 'logo_retina' );
			if( $retina_logo ):
			$style = sprintf( 'style="width:%1$s%3$s; max-height: %2$s%3$s; height: auto;"', $logo_size['width'], $logo_size['height'], 'px' );
			?>
			<img src="<?php echo $retina_logo; ?>" width="<?php echo $logo_size['width']; ?>" height="<?php echo $logo_size['height']; ?>" alt="<?php bloginfo('name'); ?>" <?php echo $style; ?> class="fusion-standard-logo fusion-logo-2x" />
			<?php else: ?>
			<img src="<?php echo fusion_get_theme_option( 'mobile_logo' ); ?>" alt="<?php bloginfo('name'); ?>" class="fusion-standard-logo fusion-logo-2x" />
			<?php endif; ?>

			<!-- mobile logo -->
			<?php if( fusion_get_theme_option( 'mobile_logo' ) ): ?>
				<img src="<?php echo fusion_get_theme_option( 'mobile_logo' ); ?>" alt="<?php bloginfo( 'name' ); ?>" class="fusion-logo-1x fusion-mobile-logo-1x" />
				<?php
				$retina_logo = fusion_get_theme_option( 'mobile_logo_retina' );
				if( $retina_logo ):
				$logo_size = fusion_get_attachment_data_by_url( fusion_get_theme_option( 'mobile_logo' ), 'mobile_logo' );
				if( ! $logo_size ) {
					$logo_size = getimagesize( fusion_get_theme_option( 'mobile_logo' ) );
					$logo_size['width'] = $logo_size[0];
					$logo_size['height'] = $logo_size[1];
				}
				$style = sprintf( 'style="max-width:%1$s%3$s; max-height: %2$s%3$s; height: auto;"', $logo_size['width'], $logo_size['height'], 'px' );
				?>
				<img src="<?php echo $retina_logo; ?>" alt="<?php bloginfo('name'); ?>" <?php echo $style; ?> class="fusion-logo-2x fusion-mobile-logo-2x" />
				<?php else: ?>
				<img src="<?php echo fusion_get_theme_option( 'mobile_logo' ); ?>" alt="<?php bloginfo( 'name' ); ?>" class="fusion-logo-2x fusion-mobile-logo-2x" />
				<?php endif; ?>
			<?php endif; ?>

			<!-- sticky header logo -->
			<?php if( fusion_get_theme_option( 'sticky_header_logo' ) && 
					( fusion_get_theme_option('header_layout') == 'v1' || fusion_get_theme_option('header_layout') == 'v2' || fusion_get_theme_option('header_layout') == 'v3' || 
					( ( fusion_get_theme_option('header_layout') == 'v4' | fusion_get_theme_option('header_layout') == 'v5' ) && fusion_get_theme_option('header_sticky_type2_layout') == 'menu_and_logo' ) ) ): ?>
				<img src="<?php echo fusion_get_theme_option( 'sticky_header_logo' ); ?>" alt="<?php bloginfo( 'name' ); ?>" class="fusion-logo-1x fusion-sticky-logo-1x" />
				<?php
				$retina_logo = fusion_get_theme_option( 'sticky_header_logo_retina' );
				if( $retina_logo ):
				$logo_size = fusion_get_attachment_data_by_url( fusion_get_theme_option( 'sticky_header_logo' ), 'sticky_header_logo' );
				if( ! $logo_size ) {
					$logo_size = getimagesize( fusion_get_theme_option( 'sticky_header_logo' ) );
					$logo_size['width'] = $logo_size[0];
					$logo_size['height'] = $logo_size[1];
				}
				$style = sprintf( 'style="max-width:%1$s%3$s; max-height: %2$s%3$s; height: auto;"', $logo_size['width'], $logo_size['height'], 'px' );
				?>
				<img src="<?php echo $retina_logo; ?>" alt="<?php bloginfo('name'); ?>" <?php echo $style; ?> class="fusion-logo-2x fusion-sticky-logo-2x" />
				<?php else: ?>
				<img src="<?php echo fusion_get_theme_option( 'sticky_header_logo' ); ?>" alt="<?php bloginfo( 'name' ); ?>" class="fusion-logo-2x fusion-sticky-logo-2x" />
				<?php endif; ?>
			<?php endif; ?>
		</a>
		<?php
		/**
		 * avada_logo_append hook
		 * @hooked avada_header_content_3 - 10
		 */
		if( fusion_get_theme_option( 'header_position' ) == 'Top' ) {
			do_action( 'avada_logo_append' );
		}
		?>
	</div>
	<?php
	$html = ob_get_clean();
	return $html;
}

add_shortcode('fusion-mobile-logo', 'sunridge_mobile_logo');
