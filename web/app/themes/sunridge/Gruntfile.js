var shell = require('shelljs');

module.exports = function(grunt) {
	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		po2mo: {
			options: {
			},
			files: {
				src: 'languages/*.po',
				expand: true,
			},
		},
		less: {
			sunridge: {
				options: {
					paths: 'assets/vendor/bootstrap/less/mixins',
					sourceMap: true,
					sourceMapFilename: 'sunridge.css.map',
					sourceMapRootpath: '/app/themes/sunridge/'
				},
				files: {
					'sunridge.css': ['assets/less/style.less', ]
				}
			}
		},
		concat: {
			options: {
				separator: ';'
			},
			development: {
				src: [
					'assets/js/jquery.scrollgallery.js',
					'assets/js/jquery.fadegallery.js',
					'assets/vendor/picturefill/picturefill.js',
					'assets/vendor/fancybox/source/jquery.fancybox.js',
					'assets/vendor/fancybox/source/helpers/jquery.fancybox-media.js',
					'assets/js/theme.js'
				],
				dest: 'assets/js/sunridge.js'
			}
		},
		uglify: {
			development: {
				options: {
					mangle: true,
					compress: {
						sequences: true,
						dead_code: true,
						conditionals: true,
						booleans: true,
						unused: true,
						if_return: true,
						join_vars: true,
						drop_console: true
					}
				},
				files: {
					'assets/js/sunridge.min.js': ['assets/js/sunridge.js']
				}
			}
		},
		watch: {
			css: {
				options: {
					livereload: false
				},
				files: ['**/*.less', '!assets/less/theme/sunridge.less'],
				tasks: ['less']
			},
			js: {
				options: {
					livereload: false
				},
				files: ['assets/js/*.js', '!assets/js/sunridge.js','!assets/js/*.min.js'],
				tasks: ['concat', 'uglify']
			},
			livereload: {
				options: {
					livereload: true
				},
				files: ['**/*.php', 'assets/js/sunridge.min.js', 'sunridge.css']
			}
		},
		svgmin: { // Task
            options: { // Configuration that will be passed directly to SVGO
                plugins: [
                    {removeViewBox: false },
                    {removeUselessStrokeAndFill: false},
                    {convertPathData: false }
                ]
            },
            webfont: {
                files: [{ // Dictionary of files
                    expand: true, // Enable dynamic expansion.
                    cwd: 'fusion-icon/icomoon', // Src matches are relative to this path.
                    src: ['*.svg'], // Actual pattern(s) to match.
                    dest: 'fusion-icon/svg', // Destination path prefix.
                    ext: '.svg' // Dest filepaths will have this extension.
                    // ie: optimise img/src/branding/logo.svg and store it in img/branding/logo.min.svg
                }]
            }
        },
		webfont: {
			icons: {
				src: ['../Avada/fusion-icon/svg/*.svg', 'fusion-icon/svg/*.svg'],
				dest: 'assets/fonts/fusion-icon',
				destCss: 'assets/less/',
				engine: 'node',
				options: {
					font: 'fusion-icon',
					//classPrefix: "fusion-icon-",
					//baseClass: "fusion-icon-",
					syntax: "bootstrap",
					types: "eot,woff,ttf,svg",
					'relativeFontPath' : 'assets/fonts/fusion-icon/',
					templateOptions: {
						baseClass: '',
						classPrefix: 'fusion-icon-',
						//mixinPrefix: 'fusion-icon-'
					},
					template: 'fusion-icon/template/template.css',
					stylesheet: "less",
					destHtml: "assets/fonts/fusion-icon",
					htmlDemoTemplate: "../Avada/fusion-icon/template/template.html",
					//ie7: true,
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-webfont');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-po2mo');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-svgmin');

	grunt.registerTask('watchCSS', ['watch:css']);
	grunt.registerTask('default', ['webfont', 'less', 'concat:development', 'uglify:development']);

	grunt.registerTask('langUpdate', 'Update languages', function() {
		shell.exec('tx pull -r avada.avadapo -a --minimum-perc=10');
		shell.exec('tx pull -r avada.fusion-corepo -a --minimum-perc=10');
		shell.exec('grunt po2mo');
	});
};