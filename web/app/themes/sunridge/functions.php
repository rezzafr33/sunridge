<?php

require_once('libs/options.php');
require_once('libs/helpers.php');
require_once('libs/shortcodes.php');

function sunridge_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/sunridge.css', array('parent-style') );
}
add_action( 'wp_enqueue_scripts', 'sunridge_enqueue_styles' );

function sunridge_enqueue_scripts(){
	$theme_info = wp_get_theme();
	wp_register_script('sunridge', get_stylesheet_directory_uri() . '/assets/js/sunridge.min.js', array('avada'), $theme_info->get( 'Version' ), true );

	wp_enqueue_script('sunridge');
}
add_action( 'wp_enqueue_scripts', 'sunridge_enqueue_scripts');


function sunridge_lang_setup() {
	$lang = get_stylesheet_directory() . '/languages';
	load_child_theme_textdomain( 'sunridge', $lang );
}
add_action( 'after_setup_theme', 'sunridge_lang_setup' );

add_action('widgets_init', 'sunridge_before_footer_sidebar', 100);

function sunridge_before_footer_sidebar() {
	register_sidebar(array(
			'name'	=> 'Before Footer 1',
			'id' => 'before-footer-1',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<div class="heading"><h3>',
			'after_title' => '</h3></div>',
		));

	register_sidebar(array(
			'name'	=> 'Before Footer 2',
			'id' => 'before-footer-2',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<div class="heading"><h3>',
			'after_title' => '</h3></div>',
		));

	register_sidebar(array(
			'name'	=> 'Before Footer 3',
			'id' => 'before-footer-3',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<div class="heading"><h3>',
			'after_title' => '</h3></div>',
		));
}

function sunridge_newsletter_default() {

	if(!defined('NEWSLETTER_VERSION'))
		return "";

	ob_start();?>
	<script type="text/javascript">
	//<![CDATA[
	if (typeof newsletter_check !== "function") {
	window.newsletter_check = function (f) {
		var re = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-]{1,})+\.)+([a-zA-Z0-9]{2,})+$/;
		if (!re.test(f.elements["ne"].value)) {
			alert("The email is not correct");
			return false;
		}
		for (var i=1; i<20; i++) {
		if (f.elements["np" + i] && f.elements["np" + i].value == "") {
			alert("");
			return false;
		}
		}
		if (f.elements["ny"] && !f.elements["ny"].checked) {
			alert("You must accept the privacy statement");
			return false;
		}
		return true;
	}
	}
	//]]>
	</script>

	<div class="newsletter newsletter-subscription">

		<form method="post" action="<?php echo plugins_url('newsletter/do/subscribe.php'); ?>" onsubmit="return newsletter_check(this)" class="form-news">
			<fieldset>
				<legend></legend>
				<strong class="title"><?php _e("Get Our Newsletter", 'sunridge'); ?></strong>
				<div class="box-form">
					<span class="input-text-icon fusion-icon-envelope"></span>
					<input class="input-text newsletter-email" type="email" name="ne" size="30" placeholder="<?php _e("Email", 'sunridge'); ?>" required>
					<input class="newsletter-submit" type="submit" value="<?php _e("Send", 'sunridge'); ?>">
				</div>
			</fieldset>
		</form>

	</div>

	<?php
	$form = ob_get_clean();
	return $form;
}
